FROM debian:9 as build
RUN apt update && apt install -y  wget gcc make libc6-dev libpcre3 libpcre3-dev zlib1g-dev
RUN wget https://github.com/openresty/luajit2/archive/refs/tags/v2.1-20220111.tar.gz && tar xzfv v2.1-20220111.tar.gz && cd luajit2-2.1-20220111 && make && make install


RUN wget https://github.com/openresty/lua-nginx-module/archive/refs/tags/v0.10.21rc1.tar.gz && tar xzfv v0.10.21rc1.tar.gz
RUN wget https://github.com/vision5/ngx_devel_kit/archive/refs/tags/v0.3.1.tar.gz && tar xzfv v0.3.1.tar.gz

RUN wget https://github.com/openresty/lua-resty-lrucache/archive/refs/tags/v0.11.tar.gz && tar xzfv v0.11.tar.gz && cd lua-resty-lrucache-0.11 && make install PREFIX=/opt/nginx
RUN wget https://github.com/openresty/lua-resty-core/archive/refs/tags/v0.1.23rc1.tar.gz && tar xzfv v0.1.23rc1.tar.gz && cd lua-resty-core-0.1.23rc1 && make install PREFIX=/opt/nginx
RUN wget https://nginx.org/download/nginx-1.19.3.tar.gz  && tar -xzvf nginx-1.19.3.tar.gz
RUN cd /nginx-1.19.3/ && export LUAJIT_LIB=//usr/local/lib && export LUAJIT_INC=/usr/local/include/luajit-2.1 &&  ./configure --with-ld-opt="-Wl,-rpath,/usr/local/lib" --add-module=/ngx_devel_kit-0.3.1 --add-module=/lua-nginx-module-0.10.21rc1  && make && make install
RUN wget -O nginx.conf  https://gitlab.com/alexeei1/less4/-/raw/main/nginx.conf?inline=false
COPY nginx.conf /usr/local/nginx/conf/


FROM debian:9 as final
RUN mkdir /opt/nginx/
COPY --from=build /usr/local/lib/libluajit-5.1.so.2 /usr/local/lib/
COPY --from=build /opt/nginx/lib/lua/resty/ /opt/nginx/lib/lua/resty/
WORKDIR /usr/local/nginx/sbin
COPY --from=build /usr/local/nginx/sbin/nginx .
RUN mkdir ../logs ../conf && touch ../logs/error.log && cd /usr/local/nginx/sbin && chmod +x nginx
CMD ["./nginx", "-g", "daemon off;"]
